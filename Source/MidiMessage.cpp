//
//  MidiMessage.cpp
//  CommandLineTool
//
//  Created by Thomas Harvey on 24/09/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#include "MidiMessage.hpp"
#include <iostream>
#include <cmath>

    MidiMessage::MidiMessage()//Constructor
    {
        number = 60;
        velocity = 127;
        channelNumber = 1;
        std::cout << "Constructor" << std::endl;
    }
    MidiMessage::~MidiMessage() //Destructor
    {
        
        std::cout << "Deconstructor!" << std::endl;
    }
    void MidiMessage::setNoteNumber (int value) //Mutator
    {
        if (value > 0 && value < 127)
            number = value;
    }
    int MidiMessage::getNoteNumber() const //Accessor
    {
        return number;
    }
    float MidiMessage::getMidiNoteInHertz() const //Accessor
    {
        return 440 * pow(2, (number-69) / 12.0);
    }
    
    void MidiMessage::setVelocity (int value) // Mutator
    {
        if (value > 0 && value < 127)
            velocity = value;
    }
    
    float MidiMessage::getNoteAmplitude () const //Accessor
    {
        return velocity/127.0;
    }
    
