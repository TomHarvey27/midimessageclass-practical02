//
//  MidiMessage.hpp
//  CommandLineTool
//
//  Created by Thomas Harvey on 24/09/2016.
//  Copyright © 2016 Tom Mitchell. All rights reserved.
//

#ifndef MidiMessage_hpp
#define MidiMessage_hpp

#include <stdio.h>

class MidiMessage
{
public:
    MidiMessage();//Constructor
    
    ~MidiMessage(); //Destructor
   
    void setNoteNumber (int value); // Sets the MIDI note number of the message
   
    int getNoteNumber() const; // Returns the MIDI note number of the message
    
    float getMidiNoteInHertz() const; // Returns the MIDI note as a frequency
    
    void setVelocity (int value); // Returns the MIDI velocity of the message
   
    float getNoteAmplitude () const; // Returns the MIDI note as an amplitude

    
    
    
private:
    int number;
    int velocity;
    int channelNumber;
};


#endif /* MidiMessage_hpp */
