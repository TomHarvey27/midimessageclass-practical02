//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include <cmath>
#include "MidiMessage.hpp"

int main (int argc, const char* argv[])
{

    MidiMessage note;
    int number,velocity;
    float frequency,amplitude;
    
    std::cout << "Enter a number" << std::endl;
    std::cin >> number;
    std::cout << "Enter a velocity" << std::endl;
    std::cin >> velocity;
    
    note.setNoteNumber(number);
    note.setVelocity(velocity);
    
    frequency = note.getMidiNoteInHertz();
    amplitude = note.getNoteAmplitude();
    
    
    std::cout << "Amplitude =  " << amplitude << " frequency = " << frequency << std::endl;
  
    note.setNoteNumber(48);
    note.getMidiNoteInHertz();

    return 0;
}

